if status is-interactive
    # Commands to run in interactive sessions can go here
end

alias pair="sudo systemctl kill -s SIGUSR1 xow"
alias config='/usr/bin/git --git-dir=$HOME/.cfg --work-tree=$HOME'
neofetch
starship init fish | source
